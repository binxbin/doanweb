﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BanGiayBLL.BLL
{
    public class GiayBLL
    {
        private BanGiayEntities db = new BanGiayEntities();

        // GET: /Giay/
        public List<GIAY> List()
        {
            //var giay = db.GIAY.Include(g => g.CHITIETSANPHAM).Include(g => g.LOAIGIAY);
            return db.GIAY.ToList();
        }

        // GET: /Giay/Details/5
        public GIAY Details(int? id)
        { 
            return db.GIAY.Find(id);  
        }

        // GET: /Giay/Create
        public void Create(GIAY giay)
        {
            db.GIAY.Add(giay);
            db.SaveChanges();
        }

      
      
        public bool Exist(int id)
        {
            return db.GIAY.Any(g => g.MaGiay==id);
            //return (db.GIAY.Find(id!=null);
        }

        //// GET: /Giay/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    GIAY giay = db.GIAY.Find(id);
        //    if (giay == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.MaGiay = new SelectList(db.CHITIETSANPHAM, "MaGiay", "MauSac", giay.MaGiay);
        //    ViewBag.MaLoaiGiay = new SelectList(db.LOAIGIAY, "MaLoai", "TenLoai", giay.MaLoaiGiay);
        //    return View(giay);
        //}

       
        public void Edit(GIAY giay)
        {
                db.Entry(giay).State = EntityState.Modified;
                db.SaveChanges();

        }

        // GET: /Giay/Delete/5
        public void Delete(int id)
        {           
            GIAY giay = db.GIAY.Find(id);
            db.GIAY.Remove(giay);
            db.SaveChanges();
        }

    }
}
