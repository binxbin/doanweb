//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BanGiayBLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHITIETSANPHAM
    {
        public int MaGiay { get; set; }
        public string MauSac { get; set; }
        public string KieuDang { get; set; }
        public string ChatLieu { get; set; }
        public string NoiSanXuat { get; set; }
        public string DeGiay { get; set; }
        public string MuiGiay { get; set; }
        public string ThongTinKhac { get; set; }
    
        public virtual GIAY GIAY { get; set; }
    }
}
