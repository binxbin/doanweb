﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BanGiayBLL;

namespace WebBanGiay.Controllers
{
    public class LoaiGiayController : Controller
    {
        private BanGiayEntities db = new BanGiayEntities();

        // GET: /LoaiGiay/
        public ActionResult Index()
        {
            return View(db.LOAIGIAY.ToList());
        }

        // GET: /LoaiGiay/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIGIAY loaigiay = db.LOAIGIAY.Find(id);
            if (loaigiay == null)
            {
                return HttpNotFound();
            }
            return View(loaigiay);
        }

        // GET: /LoaiGiay/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /LoaiGiay/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="MaLoai,TenLoai")] LOAIGIAY loaigiay)
        {
            if (ModelState.IsValid)
            {
                db.LOAIGIAY.Add(loaigiay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(loaigiay);
        }

        // GET: /LoaiGiay/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIGIAY loaigiay = db.LOAIGIAY.Find(id);
            if (loaigiay == null)
            {
                return HttpNotFound();
            }
            return View(loaigiay);
        }

        // POST: /LoaiGiay/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="MaLoai,TenLoai")] LOAIGIAY loaigiay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaigiay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(loaigiay);
        }

        // GET: /LoaiGiay/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIGIAY loaigiay = db.LOAIGIAY.Find(id);
            if (loaigiay == null)
            {
                return HttpNotFound();
            }
            return View(loaigiay);
        }

        // POST: /LoaiGiay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LOAIGIAY loaigiay = db.LOAIGIAY.Find(id);
            db.LOAIGIAY.Remove(loaigiay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
