﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BanGiayBLL;
using BanGiayBLL.BLL;

namespace WebBanGiay.Controllers
{
    public class GiayController : Controller
    {
        private GiayBLL giaybll = new GiayBLL();
        // GET: /Giay/
        public ActionResult Index()
        {
            //var giay = db.GIAY.Include(g => g.CHITIETSANPHAM).Include(g => g.LOAIGIAY);
            return View(giaybll.List());
        }

        // GET: /Giay/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GIAY giay = giaybll.Details(id);
            if (giay == null)
            {
                return HttpNotFound();
            }
            return View(giay);
        }

        //// GET: /Giay/Create
        //public ActionResult Create()
        //{
        //    ViewBag.MaGiay = new SelectList(db.CHITIETSANPHAM, "MaGiay", "MauSac");
        //    ViewBag.MaLoaiGiay = new SelectList(db.LOAIGIAY, "MaLoai", "TenLoai");
        //    return View();
        //}

        // POST: /Giay/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="MaGiay,MaLoaiGiay,TenGiay,HangSanXuat,Gia,GiaKhuyenMai,MoTa,HinhAnh")] GIAY giay)
        {
            if (ModelState.IsValid)
            {
                giaybll.Create(giay);
                return RedirectToAction("Index");
            }

            //ViewBag.MaGiay = new SelectList(db.CHITIETSANPHAM, "MaGiay", "MauSac", giay.MaGiay);
            //ViewBag.MaLoaiGiay = new SelectList(db.LOAIGIAY, "MaLoai", "TenLoai", giay.MaLoaiGiay);
            return View(giay);
        }

        // GET: /Giay/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GIAY giay = giaybll.Details(id);
            if (giay == null)
            {
                return HttpNotFound();
            }
            //ViewBag.MaGiay = new SelectList(db.CHITIETSANPHAM, "MaGiay", "MauSac", giay.MaGiay);
            //ViewBag.MaLoaiGiay = new SelectList(db.LOAIGIAY, "MaLoai", "TenLoai", giay.MaLoaiGiay);
            return View(giay);
        }

        // POST: /Giay/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="MaGiay,MaLoaiGiay,TenGiay,HangSanXuat,Gia,GiaKhuyenMai,MoTa,HinhAnh")] GIAY giay)
        {
            if (ModelState.IsValid)
            {
                giaybll.Edit(giay);
                return RedirectToAction("Index");
            }
            //ViewBag.MaGiay = new SelectList(db.CHITIETSANPHAM, "MaGiay", "MauSac", giay.MaGiay);
            //ViewBag.MaLoaiGiay = new SelectList(db.LOAIGIAY, "MaLoai", "TenLoai", giay.MaLoaiGiay);
            return View(giay);
        }

        // GET: /Giay/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GIAY giay = giaybll.Details(id);
            if (giay == null)
            {
                return HttpNotFound();
            }
            return View(giay);
        }

        // POST: /Giay/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            giaybll.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
